package svm;

import android.util.Log;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import svm.libsvm.svm;
import svm.libsvm.svm_model;
import svm.libsvm.svm_node;
import svm.libsvm.svm_scale;

public class Classifier {

    private svm_model model;
    private int TOTAL_CLASSES = 2;
    private List<double[]> supportVectors;
    private String scaled_data;


    public Classifier(BufferedReader model){
      try {
  			this.model = svm.svm_load_model(model);
  		}catch (IOException e) {
  			e.printStackTrace();
  			return;
  		}
    }

    public void scale(File file) throws Exception {
        Log.i("EVALUATING", "Scaling");
        svm_scale scale = new svm_scale();
        String[] params = {file.getAbsolutePath()};
        scale.run(params);
        scaled_data = scale.getScaled();
    }

    public double evaluate(){
        int stressSamples = 0;
        int relaxSamples = 0;
        for(int k=0; k<supportVectors.size(); k++){
            double[] sample = supportVectors.get(k);
            svm_node[] nodes = new svm_node[sample.length];
            Log.i("EVALUATING", "Creating node");
            for (int i = 0; i < sample.length; i++){
                svm_node node = new svm_node();
                node.index = i;
                node.value = sample[i];
                nodes[i] = node;
            }
            Log.i("EVALUATING", "Obtaining probability");
            int classes = svm.svm_get_nr_class(this.model);
            double[] prob_estimates = new double[classes];
            double res = svm.svm_predict_probability(model, nodes, prob_estimates);
            if(res == 1.0)
                stressSamples++;
            else if(res == 2.0)
                relaxSamples++;
        }
        return (double) stressSamples / (stressSamples + relaxSamples);
    }

    public void read(){
        Log.i("EVALUATING", "Reading");
        supportVectors = new ArrayList<double[]>();
        double[] vector;
        String[] lines = scaled_data.split("\n");
        for(int i=0; i<lines.length; i++) {
            String[] samples = lines[i].split(" ");
            vector = new double[samples.length-1];
            for(int j=1; j<samples.length-1; j++){
                String[] entry = samples[j].split(":");
                vector[j] = Double.parseDouble(entry[1]);
            }
            this.supportVectors.add(vector);
        }
    }
}
