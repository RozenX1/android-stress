package com.example.raul.androidstress.presentation.fragments;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.EditText;

import com.example.raul.androidstress.R;
import com.example.raul.androidstress.domain.Call;
import com.example.raul.androidstress.persistence.CallsConnector;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class BetweenDatesFragment extends Fragment {

    private Date start, end;
    private EditText startDate, endDate;
    private CallsConnector connector;

    public BetweenDatesFragment() {}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.connector = new CallsConnector(getActivity().getApplicationContext());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view =  inflater.inflate(R.layout.fragment_between_dates, container, false);

        this.startDate = (EditText) view.findViewById(R.id.startDate);
        this.startDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar calendar = Calendar.getInstance();
                new DatePickerDialog(getActivity(),
                        R.style.AppTheme,
                        startPickerListener,
                        calendar.get(Calendar.YEAR),
                        calendar.get(Calendar.MONTH),
                        calendar.get(Calendar.DAY_OF_MONTH)
                ).show();
            }
        });
        this.endDate = (EditText) view.findViewById(R.id.endDate);
        this.endDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar calendar = Calendar.getInstance();
                new DatePickerDialog(getActivity(),
                        R.style.AppTheme,
                        endPickerListener,
                        calendar.get(Calendar.YEAR),
                        calendar.get(Calendar.MONTH),
                        calendar.get(Calendar.DAY_OF_MONTH)
                ).show();
            }
        });
        return view;
    }

    DatePickerDialog.OnDateSetListener startPickerListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            setTime("start", year, monthOfYear, dayOfMonth);
            dataChanged();
        }
    };

    DatePickerDialog.OnDateSetListener endPickerListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            setTime("end", year, monthOfYear, dayOfMonth);
            dataChanged();
        }
    };

    private void setTime(String type, int year, int monthOfYear, int dayOfMonth) {
        Calendar calendar = Calendar.getInstance();
        calendar.clear();
        calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
        calendar.set(Calendar.MONTH, monthOfYear);
        calendar.set(Calendar.YEAR, year);
        if (type.equals("end")){
            end = calendar.getTime();
            endDate.setText(dayOfMonth+"/"+(monthOfYear+1)+"/"+year);
        }else if(type.equals("start")){
            start = calendar.getTime();
            startDate.setText(dayOfMonth+"/"+(monthOfYear+1)+"/"+year);
        }
    }

    private void dataChanged() {
        if (start == null || end == null)
            return;
        try {
            connector.open();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        ArrayList<Call> calls = connector.select_between_dates(start, end);
        Fragment frag;
        if(calls.size() == 0){
            frag = NoDataFragment.newInstance();
        }else{
            frag = DataFragment.newInstance(calls);
        }
        getFragmentManager().beginTransaction()
                .replace(R.id.data, frag)
                .commit();
    }


}
