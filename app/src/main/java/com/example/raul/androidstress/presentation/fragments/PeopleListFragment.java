package com.example.raul.androidstress.presentation.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.example.raul.androidstress.R;
import com.example.raul.androidstress.domain.Call;
import com.example.raul.androidstress.domain.Person;
import com.example.raul.androidstress.persistence.CallsConnector;
import com.example.raul.androidstress.presentation.adapters.PersonAdapter;
import com.example.raul.androidstress.presentation.webInterfaces.ArrayWebInterface;
import com.example.raul.androidstress.presentation.webInterfaces.ObjectWebInterface;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;
import java.util.ArrayList;

public class PeopleListFragment extends FragmentWithLists implements AbsListView.OnItemClickListener{

    private CallsConnector connector;
    private ListView peopleList;
    private ArrayList<Person> people;
    private WebView sigmaGraph;

    public PeopleListFragment() {}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        connector = new CallsConnector( getActivity().getApplicationContext() );
        try {
            connector.open();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        this.people = new ArrayList<>();
        for(String telephone: connector.select_telephones()){
            Person person = new Person(telephone);
            person.setCalls(connector.select_by_telephone(telephone));
            person.phoneLookup(this.getActivity());
            this.people.add(person);
        }
        connector.close();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_people, container, false);
        peopleList = (ListView) view.findViewById(android.R.id.list);
        sigmaGraph = (WebView) view.findViewById(R.id.sigmaGraph);
        fillInformation();

        return view;
    }

    private void fillInformation() {
//        People List
        PersonAdapter adapter = new PersonAdapter(getActivity(), this.people);
        peopleList.setAdapter(adapter);
        setListViewHeightBasedOnChildren(peopleList);
        peopleList.setOnItemClickListener(this);

//        Sigma Graph
        JSONObject json = new JSONObject();
        for(Person person: this.people){
            JSONArray array = new JSONArray();
            for(Call call: person.getCalls()){
                JSONObject obj = new JSONObject();
                try {
                    obj.put("duration", call.getDuration());
                    obj.put("timestamp", call.getTimestamp().getTime());
                    obj.put("stress", call.getStress());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                array.put(obj);
            }
            try {
                json.put(person.getName(), array);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        try {
            InputStream stream = this.getActivity().getAssets().open("peopleGraph.html");
            int streamSize = stream.available();
            byte[] buffer = new byte[streamSize];
            stream.read(buffer);
            stream.close();
            String html = new String(buffer);
            this.sigmaGraph.loadDataWithBaseURL("file:///android_asset/", html, "text/html", "UTF-8", null);

        } catch (IOException e) {
            e.printStackTrace();
        }

        WebSettings settings = sigmaGraph.getSettings();
        settings.setJavaScriptEnabled(true);
        sigmaGraph.addJavascriptInterface(new ObjectWebInterface(json), "SigmaData");
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Person person = this.people.get(position);
        Fragment frag = PersonFragment.newInstance(person);
        getFragmentManager().beginTransaction()
                .replace(R.id.content, frag)
                .commit();

    }

}
