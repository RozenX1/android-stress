package com.example.raul.androidstress.domain;

import android.annotation.TargetApi;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.example.raul.androidstress.R;
import com.example.raul.androidstress.persistence.CallsConnector;
import com.example.raul.androidstress.presentation.activities.MainActivity;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.sql.SQLException;
import java.util.Date;

import mfcc.Frame;
import mfcc.Params;
import svm.Classifier;
import vad.AutocorrellatedVoiceActivityDetector;
import wavfile.WavFile;
import wavfile.WavFileException;


public class VoiceProcessor extends Thread{
    private final double TRIM_THRESHOLD = 0.00006;
    private File audiofile;
    private File datafile;
    private Context context;
    private Call call;

    public VoiceProcessor(File audiofile, Context context){
        this.audiofile = audiofile;
        this.context = context;
    }

    @Override
    public void run(){
        this.call = new Call();
        double [] trimmed = trimAudio();
        extractMFCCs(trimmed);
        classify();
        add_call();
        push_notification();
    }

    private double[] trimAudio() {
        Log.i("info", "TRIMMING AUDIO");
        WavFile input = null;
        try {
            input = WavFile.openWavFile(this.audiofile);
        } catch (IOException | WavFileException e1) {
            e1.printStackTrace();
        }
        int numframes = (int)input.getNumFrames();
        double[] signal = new double[numframes];
        try {
            input.readFrames(signal, numframes);
        }catch (IOException | WavFileException e1) {
            e1.printStackTrace();
        }

        AutocorrellatedVoiceActivityDetector detector =
                new AutocorrellatedVoiceActivityDetector();
        detector.setAutocorrellationThreshold(this.TRIM_THRESHOLD);
        double[] trimmed = detector.removeSilence(signal, 11025.0f);

        try{
            WavFile output = WavFile.newWavFile(this.audiofile, 1, trimmed.length, 16, 11025);
            output.writeFrames(trimmed, trimmed.length);
            this.call.setDuration((float)output.getNumFrames()/output.getSampleRate());
            output.close();
        }catch(Exception e){
            System.err.println(e);
        }

        return trimmed;
    }

    private void extractMFCCs(double[] trimmed){
        Log.i("info", "EXTRACTING MFFCS");
        float [] signal = new float[trimmed.length];
        for(int i=0; i<signal.length; i++){
            signal[i] = (float)trimmed[i];
        }

        Frame frame_signal = new Frame(signal, Params.FS);
        frame_signal.preEmphasize(Params.PREEMPHASIZE_ALPHA);
        frame_signal.doFraming(Params.FRAME_SIZE, Params.FRAME_INIT_LAPSE, 0);
        for (Frame frame : frame_signal.getSubframes()) {
            frame.doFraming(Params.SUBFRAME_SIZE, Params.SUBFRAME_INIT_LAPSE, frame.getStart());
            for (Frame subframe : frame.getSubframes()) {
                subframe.calcMFCC(Params.NUM_FILTERS, Params.NUM_CEPSTRALS,
                        Params.NFTT, Params.MIN_FREQ);
            }
        }
        frame_signal.calcDelta();

        PrintWriter writer = null;
        try {
            String name = this.audiofile.getAbsolutePath();
            name = name.replaceFirst("wav","data");
            this.datafile = new File(name);
            writer = new PrintWriter(this.datafile, "UTF-8");
            String result = frame_signal.toSVM("1");
            String[] lines = result.split("\n");
            String output = new String();
            int times = (int)Math.ceil(lines.length/1000.0);
            for(int i=0; i<lines.length; i=i+times){
                if(!lines[i].contains("NaN")){
                    output += (lines[i]+"\n");
                }
            }


            writer.print(output);
        } catch (FileNotFoundException | UnsupportedEncodingException e) {
            e.printStackTrace();
        }finally{
            writer.close();
        }
    }

    private void classify() {
        Log.i("info", "CLASSIFYING");
        BufferedReader model = new BufferedReader(new InputStreamReader(
                context.getResources().openRawResource(R.raw.model)));
        Classifier c = new Classifier(model);
        try {
            c.scale(datafile);
        } catch (Exception e) {
            e.printStackTrace();
        }
        c.read();
        final double result = c.evaluate();
        Log.i("info", "PERCENTAGE: " + result);
        this.call.setStress((float)result);
    }


    private void add_call() {
        String[] params = datafile.getName().split("#");
        this.call.setTimestamp(new Date(Long.parseLong(params[0])));
        this.call.setTelephone_num(params[1]);
        Log.i("NEWCALL", call.toString());
        CallsConnector connector = new CallsConnector(this.context);
        try {
            connector.open();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        connector.insert_call(this.call);
        connector.close();
    }


    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    private void push_notification() {
        Intent intent = new Intent(context, MainActivity.class);
        TaskStackBuilder stack = TaskStackBuilder.create(context);
        stack.addParentStack(MainActivity.class);
        stack.addNextIntent(intent);
        PendingIntent pending = stack.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder notification = new NotificationCompat.Builder(context);
        notification.setContentTitle("New stress results!");
        notification.setContentText("The last phone call you did was successfully processed.");
        notification.setSmallIcon(R.mipmap.ic_launcher);
        notification.setContentIntent(pending);

        NotificationManager manager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        manager.notify(0, notification.build());

    }
}
