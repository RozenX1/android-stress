package com.example.raul.androidstress.presentation.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;

import com.example.raul.androidstress.R;
import com.example.raul.androidstress.domain.Call;
import com.example.raul.androidstress.persistence.CallsConnector;
import com.example.raul.androidstress.presentation.webInterfaces.DailyWebInterface;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;


public class DailyFragment extends Fragment {

    private CallsConnector connector;
    private WebView webGraph;
    private ArrayList<Call> calls;
    private ArrayList<ArrayList<Call>> orderedCalls;

    public DailyFragment() {}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.connector = new CallsConnector( getActivity().getApplicationContext() );
        try {
            connector.open();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        this.calls = connector.all();
        this.orderedCalls = new ArrayList<>();
        for(int i=0; i<24; i++){
            orderedCalls.add(new ArrayList<Call>());
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_daily, container, false);
        webGraph = (WebView) view.findViewById(R.id.dailyDoughnut);
        fillInformation();
        return view;
    }

    private void fillInformation() {
        float[] perc = new float[24];
        float[] secs = new float[24];
        for(Call call: this.calls){
            Date time = call.getTimestamp();
            Calendar cal = Calendar.getInstance();
            cal.setTime(time);
            int hour = cal.get(Calendar.HOUR_OF_DAY);
            int minutes = cal.get(Calendar.MINUTE);
            int seconds = cal.get(Calendar.SECOND);
            float duration = call.getDuration();
            int minDuration = (int)duration/60;
            this.orderedCalls.get(hour).add(call);

//            This algorithm provides an average of stress per day hour taking in account the
//            call can exceed the hour, in this case we calculate the contribution of the call to
//            each hour.
            int hops = (minutes + minDuration)/60;
            for(int i=0; i<=hops; i++){
                float hourDuration;
                if(i==0 && hops==0)
//                    Call doesn't the hour
                    hourDuration = duration;
                else if(i==0)
//                    Call exceeds and it's the first fragment of voice
                    hourDuration = (((60 - (minutes+minDuration)%60) + minDuration)%60)*60;
                else if(i==hops)
//                    Call exceeds and it's the last fragment
                    hourDuration = ((minutes + minDuration)%60)*60 + duration%60;
                else {
//                    Call exceeds and it's an intermediate fragment (hour long)
                    hourDuration = 3600;
                }

                int j = hour+i;
                float total = secs[j] + hourDuration;
                float aux = (perc[j]*secs[j]/total) + (call.getStress()*hourDuration/total);
                perc[j] = aux;
                secs[j] =+ hourDuration;
            }
        }


        JSONObject json = new JSONObject();
        for(int i=0; i<24; i++){
            if(secs[i] != 0) {
                JSONObject entry = new JSONObject();
                try {
                    entry.put("percentage", perc[i]);
                    entry.put("duration", secs[i]);
                    json.put(String.valueOf(i), entry);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }

        try {
            InputStream stream = this.getActivity().getAssets().open("dailyGraph.html");
            int streamSize = stream.available();
            byte[] buffer = new byte[streamSize];
            stream.read(buffer);
            stream.close();
            String html = new String(buffer);
            this.webGraph.loadDataWithBaseURL("file:///android_asset/", html, "text/html", "UTF-8", null);

        } catch (IOException e) {
            e.printStackTrace();
        }

        WebSettings settings = webGraph.getSettings();
        settings.setJavaScriptEnabled(true);
        webGraph.addJavascriptInterface(new DailyWebInterface(json, this), "DailyStatistics");

    }

    public void changeFilter(int hour) {
        ArrayList<Call> calls = this.orderedCalls.get(hour);
        Fragment dataFrag = DataFragment.newInstance(calls);
        getFragmentManager().beginTransaction()
                .replace(R.id.data, dataFrag)
                .commit();
    }
}
