package com.example.raul.androidstress.domain;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Date;


public class Call implements Parcelable {

    private Date timestamp;
    private String telephone_num;
    private float stress;
    public float duration;

    public void setDuration(float duration) {this.duration = duration;}
    public float getDuration() {return duration;}
    public Date getTimestamp() {return timestamp;}
    public void setTimestamp(Date timestamp) {this.timestamp = timestamp;}
    public String getTelephone_num() {return telephone_num;}
    public void setTelephone_num(String telephone_num) {this.telephone_num = telephone_num;}
    public float getStress() {return stress;}
    public void setStress(float stress) {this.stress = stress;}

    public Call(){}
    public Call(Date timestamp, String telephone_num, float stress, float duration) {
        this.timestamp = timestamp;
        this.telephone_num = telephone_num;
        this.stress = stress;
        this.duration = duration;
    }

    public String toString(){
        return "{TIMESTAMP: "+timestamp.toString()+", TELEPHONE_NUM: "+telephone_num+", STRESS: "+
                stress+", DURATION: "+duration+"}";
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.timestamp.getTime());
        dest.writeString(this.telephone_num);
        dest.writeFloat(this.duration);
        dest.writeFloat(this.stress);
    }
}
