package com.example.raul.androidstress.persistence;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.raul.androidstress.domain.Call;
import com.example.raul.androidstress.domain.Person;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;


public class CallsConnector {

    private static final String DB_NAME = "StressDB";
    private static final String CALLS_TABLE = "Calls";
    private CallsSQLiteHelper helper;
    private SQLiteDatabase db;

    public CallsConnector(Context context){
        helper = new CallsSQLiteHelper(context, DB_NAME, null, 1);
    }

    public CallsConnector open() throws SQLException {
        db = helper.getWritableDatabase();
        return this;
    }

    public void close(){
        if(db != null) db.close();
    }

    public long insert_call(Call call){
        ContentValues new_call = new ContentValues();
        new_call.put("telephone_num", call.getTelephone_num());
        new_call.put("timestamp", call.getTimestamp().getTime());
        new_call.put("duration", call.getDuration());
        new_call.put("stress", call.getStress());
        return db.insert(CALLS_TABLE, null, new_call);
    }

    public ArrayList<Call> all(){
        Cursor c = db.rawQuery("SELECT * FROM Calls;", null);
        return fromCursor(c);
    }

    public ArrayList<String> select_telephones(){
        Cursor c = db.rawQuery("SELECT telephone_num FROM Calls GROUP BY telephone_num;", null);
        ArrayList<String> results = new ArrayList<>();
        if(c.moveToFirst()){
            do{
                results.add(c.getString(0));
            }while(c.moveToNext());
        }
        return results;
    }

    public ArrayList<Call> select_by_telephone(String telephone){
        String[] args = {telephone};
        Cursor c = db.query("Calls", null, "telephone_num=?", args, null, null, null);
        return fromCursor(c);
    }

    public ArrayList<Call> select_between_dates(Date firstDate, Date lastDate){
        String[] args = {String.valueOf(firstDate.getTime()), String.valueOf(lastDate.getTime())};
        Cursor c = db.rawQuery("SELECT * FROM Calls WHERE timestamp BETWEEN ? AND ?", args);
        return fromCursor(c);
    }

    private ArrayList<Call> fromCursor(Cursor c){
        ArrayList<Call> results = new ArrayList<>();
        if(c.moveToFirst()){
            do{
                Date timestamp = new Date(c.getLong(0));
                String telephone_num = c.getString(1);
                float stress = c.getFloat(2);
                float duration = c.getFloat(3);
                results.add(new Call(timestamp, telephone_num, stress, duration));
            }while(c.moveToNext());
        }
        return results;
    }

}
