package com.example.raul.androidstress.presentation.adapters;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.raul.androidstress.R;
import com.example.raul.androidstress.domain.Call;
import com.example.raul.androidstress.domain.Person;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;


public class PersonAdapter extends ArrayAdapter {

    private Activity context;
    private ArrayList<Person> people;

    public PersonAdapter(Activity context, ArrayList<Person> people) {
        super(context, R.layout.person_item, people);
        this.context = context;
        this.people = people;
    }


    public View getView(int position, View convertView, ViewGroup parent){
        LayoutInflater inflater = context.getLayoutInflater();
        View item = inflater.inflate(R.layout.person_item, null);
        TextView numberOfCallsText = (TextView) item.findViewById(R.id.numberOfCallsText);
        TextView nameText = (TextView) item.findViewById(R.id.nameText);

        Person person = people.get(position);
        numberOfCallsText.setText(String.valueOf(person.getCalls().size()));
        nameText.setText(person.getName());

        return item;
    }



}
