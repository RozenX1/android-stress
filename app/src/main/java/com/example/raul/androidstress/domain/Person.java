package com.example.raul.androidstress.domain;


import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import android.provider.ContactsContract;

import java.util.ArrayList;

public class Person implements Parcelable {

    private String name;
    private String telephoneNumber;
    private ArrayList<Call> calls;

    public Person(String telephone) {
        this.telephoneNumber = telephone;
    }

    public ArrayList<Call> getCalls() {return calls;}
    public void setCalls(ArrayList<Call> calls) {this.calls = calls;}
    public String getTelephoneNumber() {return telephoneNumber;}
    public String getName() {
        if(this.name != null)
            return name;
        else
            return telephoneNumber;
    }
    public void setTelephoneNumber(String telephoneNumber) {this.telephoneNumber = telephoneNumber;}

    public void phoneLookup(Context context){
        ContentResolver cr = context.getContentResolver();
        Uri uri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI,
                Uri.encode(this.telephoneNumber));
        Cursor cursor = cr.query(uri, new String[]{ContactsContract.PhoneLookup.DISPLAY_NAME},
                null, null, null);
        if (cursor == null) {
            return;
        }
        if(cursor.moveToFirst()) {
            this.name = cursor.getString(cursor.getColumnIndex(
                    ContactsContract.PhoneLookup.DISPLAY_NAME));
        }
        if(cursor != null && !cursor.isClosed()) {
            cursor.close();
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.name);
        dest.writeString(this.telephoneNumber);
        dest.writeList(this.calls);
    }


}
