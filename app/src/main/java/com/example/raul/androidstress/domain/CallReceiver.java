package com.example.raul.androidstress.domain;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;
import java.util.Date;

public class CallReceiver extends BroadcastReceiver{
    private static Recording recording;
    private Bundle bundle;
    private String state;
    private static boolean wasOutgoing = false;
    private File audiofile;
    private String telephone_num;



    @Override
    public void onReceive(Context context, Intent intent) {
        if ((bundle = intent.getExtras()) != null) {
            state = bundle.getString(TelephonyManager.EXTRA_STATE);
            String phone;
            if((phone = bundle.getString("incoming_number")) == null){return;}
            this.telephone_num = phone;
            if (state.equals(TelephonyManager.EXTRA_STATE_OFFHOOK)) {
                startCall(context);
            }else if (state.equals(TelephonyManager.EXTRA_STATE_IDLE))
                finishCall(context);
        }

    }

    private void startCall(Context context) {
        // Maybe two OFFHOOK broadcast can be sent at the beginning.
        Log.d("Broadcast","RECORDING: wasOutgoing -> "+wasOutgoing);
        if(wasOutgoing)
            return;
        wasOutgoing = true;
        File dir = new File(Environment.getExternalStorageDirectory(), "/StressRecordings");
        if (!dir.exists()) {
            dir.mkdirs();
        }
        String name = Long.toString((new Date()).getTime());
        name += "#"+this.telephone_num+"#";
        try {
            audiofile = File.createTempFile(name, ".wav", dir);
            recording = new Recording(audiofile.getAbsolutePath());
        } catch (IOException e) {
            e.printStackTrace();
        }
        Toast.makeText(context, "RECORDING: "+audiofile.getAbsolutePath(), Toast.LENGTH_LONG).show();
    }


    private void finishCall(Context context) {
        Log.d("Broadcast", "STOP RECORDING: wasOutgoing -> " + wasOutgoing);
        if(wasOutgoing){
            wasOutgoing = false;
            recording.finish();
            try {
                recording.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            VoiceProcessor processor = new VoiceProcessor(audiofile, context);
            processor.start();
            Toast.makeText(context, "STOP RECORDING", Toast.LENGTH_SHORT).show();
        }
    }


}
