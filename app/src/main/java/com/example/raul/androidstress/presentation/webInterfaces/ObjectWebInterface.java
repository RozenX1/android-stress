package com.example.raul.androidstress.presentation.webInterfaces;


import android.webkit.JavascriptInterface;

import org.json.JSONObject;

public class ObjectWebInterface {

    private JSONObject json;

    public ObjectWebInterface(JSONObject json){
        this.json = json;
    }

    @JavascriptInterface
    public String getJSON(){
        return json.toString();
    }

}
