package com.example.raul.androidstress.domain;

import android.media.AudioFormat;
import android.media.AudioRecord;
import android.media.MediaRecorder;
import android.util.Log;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import wavfile.WavFile;

public class Recording extends Thread{
    private AudioRecord recorder;
    private boolean stopped = false;
    private List<short[]> buffers;
    private final static int SAMPLERATE = 11025;
    private final static int CHANNELS = 1;
    private final static int BITSSAMPLE = 16;
    private final static int MINBUFFERSIZE = 1024;
    private File file;

    public Recording(String path) throws IOException{
        this.file = new File(path);
        android.os.Process.setThreadPriority(android.os.Process.THREAD_PRIORITY_URGENT_AUDIO);
        start();
    }

    @Override
    public void run(){
        try{
            recorder = new AudioRecord(MediaRecorder.AudioSource.MIC, SAMPLERATE,
                    AudioFormat.CHANNEL_IN_MONO, AudioFormat.ENCODING_PCM_16BIT, MINBUFFERSIZE*BITSSAMPLE/2);
            buffers = new ArrayList<short[]>();
            recorder.startRecording();
            while(!stopped){
                short[] buffer = new short[MINBUFFERSIZE];
                Integer total_bytes = recorder.read(buffer, 0, buffer.length);
                Log.d("Total bytes", total_bytes.toString());
                buffers.add(buffer);
            }
        }catch(Exception e){
            Log.d("Audio", "Error reading voice audio");
        }finally{
            recorder.stop();
            recorder.release();
        }
        save();
    }

    public void finish()
    {
        Log.d("Audio", "Stopping");
        stopped = true;
    }

    private void save(){
        // First part is unifying the samples
        Log.d("Audio", "Saving!");
        int[] samples = new int[MINBUFFERSIZE*buffers.size()];
        for(int i=0; i<buffers.size(); i++){
            short[] buf = buffers.get(i);
            for(int j=0;j<buf.length; j++){
                samples[i*MINBUFFERSIZE+j] = (int)buf[j];
            }
        }
        // Write them to file
        try{
            WavFile output = WavFile.newWavFile(this.file, CHANNELS, samples.length, BITSSAMPLE, SAMPLERATE);
            output.writeFrames(samples, samples.length);
            output.close();
        }catch(Exception e){
            System.err.println(e);
        }
    }

}
