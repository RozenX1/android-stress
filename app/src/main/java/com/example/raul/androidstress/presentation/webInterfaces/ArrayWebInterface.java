package com.example.raul.androidstress.presentation.webInterfaces;


import android.webkit.JavascriptInterface;

import org.json.JSONArray;

public class ArrayWebInterface {

    private JSONArray json;

    public ArrayWebInterface(JSONArray json){
        this.json = json;
    }

    @JavascriptInterface
    public String getJSON(){
        return json.toString();
    }

}
