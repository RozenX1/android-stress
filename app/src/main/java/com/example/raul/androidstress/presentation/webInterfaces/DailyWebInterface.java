package com.example.raul.androidstress.presentation.webInterfaces;


import android.webkit.JavascriptInterface;

import com.example.raul.androidstress.presentation.fragments.DailyFragment;

import org.json.JSONObject;

public class DailyWebInterface {

    private JSONObject json;
    private DailyFragment frag;

    public DailyWebInterface(JSONObject json, DailyFragment frag){
        this.json = json;
        this.frag = frag;
    }

    @JavascriptInterface
    public String getJSON(){
        return json.toString();
    }

    @JavascriptInterface
    public void changeFilter(int hour){
        frag.changeFilter(hour);
    }
}
