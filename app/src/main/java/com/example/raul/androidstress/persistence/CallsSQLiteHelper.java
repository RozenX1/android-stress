package com.example.raul.androidstress.persistence;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;


public class CallsSQLiteHelper extends SQLiteOpenHelper {

    private String sql_create_table = "CREATE TABLE Calls(timestamp INT, telephone_num STRING, stress FLOAT, duration FLOAT);";

    public CallsSQLiteHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        try{
            db.execSQL(sql_create_table);
        }catch(SQLException e){
            e.printStackTrace();
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        try {
            db.execSQL("DROP TABLE IF EXISTS Calls");
            db.execSQL(sql_create_table);
        } catch (SQLException e){
            e.printStackTrace();
        }
    }
}
