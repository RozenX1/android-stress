package com.example.raul.androidstress.presentation.customViews;


import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.webkit.WebView;

public class ScrollingWebView extends WebView {

    public ScrollingWebView(Context context) {
        super(context);
    }

    public ScrollingWebView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ScrollingWebView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event){
        requestDisallowInterceptTouchEvent(true);
        return super.onTouchEvent(event);
    }
}