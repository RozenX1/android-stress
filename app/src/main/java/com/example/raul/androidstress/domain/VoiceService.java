package com.example.raul.androidstress.domain;

import android.app.Service;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.IBinder;
import android.telephony.TelephonyManager;

import com.example.raul.androidstress.domain.CallReceiver;

public class VoiceService extends Service {
    private CallReceiver callReceiver;

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if(this.callReceiver == null){
            IntentFilter filter = new IntentFilter();
            filter.addAction(TelephonyManager.ACTION_PHONE_STATE_CHANGED);
            this.callReceiver = new CallReceiver();
            registerReceiver(this.callReceiver, filter);
        }
        return START_STICKY;
    }

}
