package com.example.raul.androidstress.presentation.fragments;

import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.raul.androidstress.R;
import com.example.raul.androidstress.domain.Call;
import com.example.raul.androidstress.domain.Person;
import com.example.raul.androidstress.persistence.CallsConnector;

import java.sql.SQLException;
import java.util.ArrayList;

public class PersonFragment extends Fragment {

    private Person person;
    private TextView personLbl;
    private final static String key = "PERSON";

    public static PersonFragment newInstance(Person person) {
        PersonFragment fragment = new PersonFragment();
        Bundle args = new Bundle();
        args.putParcelable(key, person);
        fragment.setArguments(args);
        return fragment;
    }

    public PersonFragment(){}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            this.person = getArguments().getParcelable(key);
        }
        Fragment dataFrag = DataFragment.newInstance(person.getCalls());
        getFragmentManager().beginTransaction()
                .replace(R.id.data, dataFrag)
                .commit();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_person, container, false);
        personLbl = (TextView) view.findViewById(R.id.personLbl);
        if(person.getName() == null){
            personLbl.setText(person.getTelephoneNumber());
        }else{
            personLbl.setText(person.getName());
        }
        return view;
    }

}
