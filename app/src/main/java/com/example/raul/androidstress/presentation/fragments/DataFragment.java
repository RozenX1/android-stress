package com.example.raul.androidstress.presentation.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ListView;
import android.widget.TextView;

import com.example.raul.androidstress.R;
import com.example.raul.androidstress.domain.Call;
import com.example.raul.androidstress.presentation.adapters.CallAdapter;
import com.example.raul.androidstress.presentation.webInterfaces.ArrayWebInterface;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;


public class DataFragment extends FragmentWithLists {

    private ArrayList<Call> calls;
    private final static String keyCalls = "CALLS";
    private TextView numberOfCalls;
    private ListView listCalls;
    private WebView webGraph;

    public static DataFragment newInstance(ArrayList<Call> calls) {
        DataFragment fragment = new DataFragment();
        Bundle args = new Bundle();
        args.putParcelableArrayList(keyCalls, calls);
        fragment.setArguments(args);
        return fragment;
    }

    public DataFragment() {}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            this.calls = getArguments().getParcelableArrayList(keyCalls);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_data, container, false);
        this.numberOfCalls = (TextView) view.findViewById(R.id.numberOfCalls);
        this.listCalls = (ListView) view.findViewById(R.id.listCalls);
        this.webGraph = (WebView) view.findViewById(R.id.webGraph);
        fillInformation();
        return view;
    }

    private void fillInformation() {
//        Graph
        JSONArray jsonStress = new JSONArray();
        JSONArray jsonDuration = new JSONArray();
        for(int i=0; i<calls.size(); i++){
            Call call = calls.get(i);
            JSONObject jsonObjStress = new JSONObject ();
            JSONObject jsonObjDuration = new JSONObject ();
            try {
                jsonObjStress.put("y", call.getStress());
                jsonObjStress.put("x", call.getTimestamp().getTime());
                jsonObjDuration.put("y", call.getDuration());
                jsonObjDuration.put("x", call.getTimestamp().getTime());
            } catch (JSONException e) {
                e.printStackTrace();
            }
            jsonDuration.put(jsonObjDuration);
            jsonStress.put(jsonObjStress);
        }
        this.numberOfCalls.setText(String.valueOf(this.calls.size()));

        CallAdapter adapter = new CallAdapter(getActivity(), calls);
        listCalls.setAdapter(adapter);
        setListViewHeightBasedOnChildren(listCalls);

        try {
            InputStream stream = this.getActivity().getAssets().open("datesGraph.html");
            int streamSize = stream.available();
            byte[] buffer = new byte[streamSize];
            stream.read(buffer);
            stream.close();
            String html = new String(buffer);
            this.webGraph.loadDataWithBaseURL("file:///android_asset/", html, "text/html", "UTF-8", null);

        } catch (IOException e) {
            e.printStackTrace();
        }

        WebSettings settings = webGraph.getSettings();
        settings.setJavaScriptEnabled(true);
        webGraph.addJavascriptInterface(new ArrayWebInterface(jsonStress), "DataStress");
        webGraph.addJavascriptInterface(new ArrayWebInterface(jsonDuration), "DataDuration");

    }



}
