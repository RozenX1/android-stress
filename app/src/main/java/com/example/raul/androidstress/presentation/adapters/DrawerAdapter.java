package com.example.raul.androidstress.presentation.adapters;


import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.raul.androidstress.R;

public class DrawerAdapter extends ArrayAdapter {

    Activity context;
    String[] options;

    public DrawerAdapter(Activity context, String[] options) {
        super(context, R.layout.drawer_item_, options);
        this.context = context;
        this.options = options;
    }

    public View getView(int position, View converView, ViewGroup parent){
        LayoutInflater inflater = this.context.getLayoutInflater();

        View item = inflater.inflate(R.layout.drawer_item_, null);
        String option = options[position];

        TextView content = (TextView)item.findViewById(R.id.textView);
        content.setText(option);

        ImageView image = (ImageView)item.findViewById(R.id.imageView);
        int imageResource = 0;
        switch(position){
            case 0:
                imageResource = R.drawable.people;
                break;
            case 1:
                imageResource = R.drawable.clock;
                break;
            case 2:
                imageResource = R.drawable.calendar;
                break;
        }
        image.setImageResource(imageResource);

        return (item);
    }

}
