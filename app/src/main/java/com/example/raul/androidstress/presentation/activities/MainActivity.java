package com.example.raul.androidstress.presentation.activities;

import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.content.res.Configuration;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ScrollView;

import com.example.raul.androidstress.R;
import com.example.raul.androidstress.domain.VoiceService;
import com.example.raul.androidstress.presentation.fragments.BetweenDatesFragment;
import com.example.raul.androidstress.presentation.fragments.DailyFragment;
import com.example.raul.androidstress.presentation.fragments.PeopleListFragment;
import com.example.raul.androidstress.presentation.adapters.DrawerAdapter;


public class MainActivity extends AppCompatActivity{

    private ListView drawerList;
    private DrawerLayout drawerLayout;
    private LinearLayout drawerLinear;
    private CustomActionBarDrawerToggle drawerToggle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // Start service
        Intent intent = new Intent(MainActivity.this, VoiceService.class);
        startService(intent);
        // Left Drawer
        String[] drawer_options = {"Contacts", "Daily", "Period"};
        this.drawerList = (ListView) findViewById(R.id.left_drawer);
        drawerList.setAdapter(new DrawerAdapter(this, drawer_options));
        drawerList.setOnItemClickListener(new DrawerItemClickListener());

        this.drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        this.drawerToggle = new CustomActionBarDrawerToggle(this, drawerLayout,
                R.string.drawer_open, R.string.drawer_close);
        this.drawerLayout.setDrawerListener(this.drawerToggle);
        this.drawerLinear = (LinearLayout) findViewById(R.id.drawer_view);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        selectItem(0);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        drawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        drawerToggle.onConfigurationChanged(newConfig);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
//        boolean drawerOpen = this.drawerLayout.isDrawerOpen(this.drawerList);
//        menu.findItem(R.id.action_websearch).setVisible(!drawerOpen);
        return super.onPrepareOptionsMenu(menu);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }else if(drawerToggle.onOptionsItemSelected(item)){
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void selectItem(int position) {
        Fragment frag = null;
        switch(position){
            case 0:
                frag = new PeopleListFragment();
                break;
            case 1:
                frag = new DailyFragment();
//                getActionBar().setTitle("Statistics by people");
                break;
            case 2:
                frag = new BetweenDatesFragment();
                break;
        }
        drawerList.setItemChecked(position, true);
        drawerLayout.closeDrawer(drawerLinear);
        getFragmentManager().beginTransaction()
                .replace(R.id.content, frag)
                .commit();

    }

    private class DrawerItemClickListener implements ListView.OnItemClickListener {

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            selectItem(position);
        }
    }

    private class CustomActionBarDrawerToggle extends ActionBarDrawerToggle {

        public CustomActionBarDrawerToggle(Activity activity, DrawerLayout drawerLayout, int openDrawerContentDescRes, int closeDrawerContentDescRes) {
            super(activity, drawerLayout, openDrawerContentDescRes, closeDrawerContentDescRes);
        }

        public void onDrawerClosed(View view){
            super.onDrawerClosed(view);
//            getActionBar().setTitle(blablabla);
            invalidateOptionsMenu();
        }

        public void onDrawerOpened(View drawerView){
            super.onDrawerOpened(drawerView);
//            getActionBar().setTitle(blablabla);
            invalidateOptionsMenu();
        }
    }
}
