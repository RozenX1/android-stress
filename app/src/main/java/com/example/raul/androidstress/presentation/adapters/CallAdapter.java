package com.example.raul.androidstress.presentation.adapters;

import android.app.Activity;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.raul.androidstress.R;
import com.example.raul.androidstress.domain.Call;
import com.example.raul.androidstress.domain.Person;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

public class CallAdapter extends ArrayAdapter {
    private Activity context;
    private ArrayList<Call> calls;
    private static int stressColor = Color.parseColor("#FF5333");
    private static int relaxColor = Color.parseColor("#009ACD");

    public CallAdapter(Activity context, ArrayList<Call> calls){
        super(context, R.layout.call_item, calls);
        this.context = context;
        this.calls = calls;
    }

    public View getView(int position, View convertView, ViewGroup parent){
        LayoutInflater inflater = context.getLayoutInflater();
        View item = inflater.inflate(R.layout.call_item, null);
        item.setOnClickListener(null);
        ProgressBar bar = (ProgressBar)item.findViewById(R.id.stressBar);
        TextView durationText = (TextView) item.findViewById(R.id.durationText);
        TextView stressText = (TextView) item.findViewById(R.id.stressText);
        TextView dateText = (TextView) item.findViewById(R.id.dateText);
        TextView telephoneText = (TextView) item.findViewById(R.id.telephoneText);

        Call call = calls.get(position);

        stressText.setText(String.valueOf((int) (call.getStress() * 100)) + "% of time stressed");
        int hours = (int)(call.getDuration()/3600);
        int mins = (int)((call.getDuration()%3600)/60);
        int secs = (int)((call.getDuration()%3600)%60);
        durationText.setText(hours+"h "+mins+"m "+secs+"s");
        bar.setProgress((int) (call.getStress() * 100));
        DateFormat format = new SimpleDateFormat("MM/dd/yyyy");
        dateText.setText(format.format(call.getTimestamp()));

        Person person = new Person(call.getTelephone_num());
        person.phoneLookup(context);
        if(person.getName() == null)
            telephoneText.setText(person.getTelephoneNumber());
        else
            telephoneText.setText(person.getName());

         int red = (int)(call.getStress() * Color.red(stressColor) +
                (1 - call.getStress()) * Color.red(relaxColor));
        int green = (int)(call.getStress() * Color.green(stressColor) +
                (1 - call.getStress()) * Color.green(relaxColor));
        int blue = (int)(call.getStress() * Color.blue(stressColor) +
                (1 - call.getStress()) * Color.blue(relaxColor));
        int fadeColor = Color.rgb(red,green,blue);
        stressText.setTextColor(fadeColor);

        return item;
    }


}
