package mfcc;

public class Cepstrogram {
	private float [][]cepstrogram;
	
	public Cepstrogram(int numFrames, int numMelCepstrals){
		this.cepstrogram = new float[numMelCepstrals][numFrames];
		//cepstrogram is initialized with zeros
	}
	
	//Copy:
	public Cepstrogram(float [][]cepstrogram){
		 this.cepstrogram = new float[cepstrogram.length][cepstrogram[0].length];
		 for(int i=0; i< cepstrogram.length; i++){
			 for(int j=0; j< cepstrogram[i].length; j++){
				 this.cepstrogram[i][j] = cepstrogram[i][j];
			 }
		 }
	}
	
	public void add(int index, float [] frameMelCepstrals){
		for(int j=0; j<cepstrogram.length; j++){
			this.cepstrogram[j][index]=frameMelCepstrals[j];
		}
	}
	
	public float [][] getCepstrogram(){
		return this.cepstrogram;
	}
	
	public String toString(){
		String result= new String();
		for(int i=0; i< cepstrogram.length; i++){
			 for(int j=0; j< cepstrogram[i].length; j++){
				 result+= cepstrogram[i][j]+",\t";
			 }
			 result+="\n";
		}
		return result;
	}
}
