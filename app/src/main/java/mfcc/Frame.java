package mfcc;

import java.util.ArrayList;
import java.util.List;


public class Frame {
	private float[] frameSignal;
	private int Fs;
	private List<Frame> subframes;
	private float energy;
	private float zeroCross;
	private int numMelCepstrals;
	private float[] melCepstralCoeffs;
	private float[] deltaCoeffs;
	private float[] ddeltaCoeffs;
	private int start;
	private int end;

	public Frame(float[] frameSignal, int Fs) {
		this.frameSignal = frameSignal;
		this.Fs = Fs;
	}
	public Frame(float[] frameSignal, int Fs, int start,int end) {
		this.frameSignal = frameSignal;
		this.Fs = Fs;
		this.start = start;
		this.end = end;
	}
	public void preEmphasize(float alpha) {
		// Apply pre-emphasis to each sample
		for (int i = frameSignal.length - 1; i >= 1; i--) {
			frameSignal[i] = frameSignal[i] - alpha * frameSignal[i - 1];
		}
	}

	public void doFraming(int subframeSize, int subframeHopSize, int iniFrame) {
		subframes = new ArrayList<Frame>();
		int frameSize = frameSignal.length;
		int ini = 0, fin = ini + subframeSize;
		// Ya toma en cuenta el solapamiento
		while (fin <= frameSize) {
			float samples[] = new float[subframeSize];
			for (int j = 0; j < subframeSize; j++) {
				samples[j] = frameSignal[ini + j];
			}
			subframes.add(new Frame(samples, Fs,ini+iniFrame,fin+iniFrame));
			ini += subframeHopSize;
			fin += subframeHopSize;
		}
		if ((fin - subframeHopSize) < frameSize) {
			float samples[] = new float[subframeSize];
			int j;
			for ( j= 0; j < subframeSize + frameSize - fin; j++) {
				samples[j] = frameSignal[ini + j];
			}
			// Zero padding last frame, not explicitly but java
			// initialize the rest of the array to 0.0 by default.
			// for ( ; j<subframeSize ; j++){
			// 	samples[j] = 0f;
			// }
			subframes.add(new Frame(samples, Fs,ini+iniFrame,fin+iniFrame));
		}
	}

	public void calcEnergy() {
		float sumatorio = 0;
		for (int i = 0; i < frameSignal.length; i++) {
			sumatorio += Math.pow(frameSignal[i], 2);
		}
		// En decibelios: (-INF si todos los samples del frame son 0)
		setEnergy(10 * (float) Math.log10(sumatorio / frameSignal.length));
	}

	public void calcZCR() { // (0 si todos los samples del frame son 0)
		float sumatorio = 0;
		for (int i = 1; i < frameSignal.length; i++) {
			sumatorio += 0.5f * Math.abs(Math.signum(frameSignal[i])
					- Math.signum(frameSignal[i - 1]));
		}
		setZCR(sumatorio / frameSignal.length);
	}

	public void calcMFCC(int numFilters, int numCepstrals, int nftt, int lowerFreqHz) {
		this.numMelCepstrals = numCepstrals;
		deltaCoeffs = new float[this.numMelCepstrals];
		MFCC mfcc = new MFCC(numFilters, this.numMelCepstrals, this.Fs, nftt, lowerFreqHz, this.frameSignal);
		this.melCepstralCoeffs = mfcc.calcMFCC();
	}

	public List<Frame> getSubframes() {
		return subframes;
	}

	public float[] getMelCepstralcoeffs() {
		return melCepstralCoeffs;
	}

	public float getEnergy() {
		return energy;
	}

	public float getZCR() {
		return zeroCross;
	}

	public int getStart(){
		return start;
	}

	public int getEnd(){
		return end;
	}

	public void setEnergy(float nEnergy) {
		this.energy = nEnergy;
	}

	public void setZCR(float nZCR) {
		this.zeroCross = nZCR;
	}

	public String toString() {
		String frameString = new String();
		// for(int i=0; i<frameSignal.length; i++){
		// frameString+=frameSignal[i]+" ";
		// }
		frameString += "\n Energy: " + getEnergy() + " ZCR: " + getZCR();
		frameString+="\n melCepstralCoeffs: ";
		for(int i=0; i<melCepstralCoeffs.length; i++){
			frameString+= melCepstralCoeffs[i] + ", ";
		}
		return frameString;
	}

	public void calcDelta(){
		innerCalcDelta(null, null);
	}

	private void innerCalcDelta(Frame previousMFCC, Frame nextMFCC){
		// It's the smallest frame posible.
		if(this.subframes == null){
			// Remember: The very first and very last deltas are equal to zero, That's a shame.
			if(previousMFCC == null || nextMFCC == null)
				return;
			for(int i=0; i<this.numMelCepstrals-1; i++){
				// Di [ n ] = MFFCi [ n + M ] - MFFCi [ n - M ]
				deltaCoeffs[i] = nextMFCC.getMelCepstralcoeffs()[i] -
					previousMFCC.getMelCepstralcoeffs()[i];
			}
		// It's a bigger one.
		}else{
			// We need the hop value M (plus or minus M).
			int M = 1;
			// If the next level is the last level we need M=3, M=1 otherwise.
			if(this.subframes.get(0).getSubframes() == null){
				M = 3;
			}
			int size = this.subframes.size();
			for(int i=0; i<size; i++){
				// This case is not problematic
				if(M <= i && i <= size-1-M){
					this.subframes.get(i).innerCalcDelta(this.subframes.get(i-M), this.subframes.get(i+M));
				// This one have to check the previous frame in order to link
				}else if(i < M){
					// If the previous frame is null we are at the very boundary.
					if(previousMFCC == null)
						this.subframes.get(i).innerCalcDelta(null, subframes.get(i));
					else{
						Frame previous = previousMFCC.getSubframes().get(i - M + size);
						this.subframes.get(i).innerCalcDelta(previous, this.subframes.get(i+M));
					}
				// This one have to check the next frame in order to link
				}else{
					// If the next frame is null we are at the very boundary.
					if(nextMFCC == null)
						this.subframes.get(i).innerCalcDelta(subframes.get(i), null);
					else{
						Frame next = nextMFCC.getSubframes().get(i + M - size);
						this.subframes.get(i).innerCalcDelta(subframes.get(i-M), next);
					}
				}
			}
		}
	}

	public void calcDDelta(){
		// TODO
	}

	public String mfcc2csv(){
		String csv = new String();
		if(this.subframes == null){
			// Smallest frame with 12 cepstrals,
			// join timestamp and MFCC with commas.
			csv += ((float)this.start)/(Fs); // We need time, no samples
			for(int i=0; i<this.melCepstralCoeffs.length; i++){
				csv += ", " + this.melCepstralCoeffs[i];
			}
			csv += "\n";
		}else{
			// Huge frame composed by smaller frames
			for (Frame subframe : this.subframes) {
				csv += subframe.mfcc2csv();
			}
		}
		return csv;
	}

	public String delta2csv(){
		String csv = new String();
		if(this.subframes == null){
			// Smallest frame with 12 delta,
			// join timestamp and delta with commas.
			csv += ((float)this.start)/(Fs); // We need time, no samples
			for(int i=0; i<this.deltaCoeffs.length; i++){
				csv += ", " + this.deltaCoeffs[i];
			}
			csv += "\n";
		}else{
			// Huge frame composed by smaller frames
			for (Frame subframe : this.subframes) {
				csv += subframe.delta2csv();
			}
		}
		return csv;
	}

	public String toSVM(String label){
		// Turn into SVM data format
		// The label points out one arbitrary type
		String data = new String();
		if(this.subframes == null){
			int i, acc = 1;
			data += label;
			// For mel coefficients
			for(i=0; i<this.melCepstralCoeffs.length; i++){
				data += " " + acc + ":" + this.melCepstralCoeffs[i];
				acc++;
			}
			// For delta coefficients
			for(i=0; i<this.deltaCoeffs.length; i++){
				data += " " + acc + ":" + this.deltaCoeffs[i];
				acc++;
			}
			calcZCR();
			data += " " + (acc++) + ":" + getZCR();
			calcEnergy();
			data += " " + (acc++) + ":" + getEnergy();
			data += "\n";
		}else{
			for(Frame subframe: this.subframes){
				data += subframe.toSVM(label);
			}
		}
		return data;
	}
}
