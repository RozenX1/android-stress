package mfcc;


public class Params{
	/*
		Class for general constants and parameters 
	*/

	// Frecuencies, 
	public static final int MIN_FREQ = 50;
	public static final int FS = 11025; //Double of the max frecuency which can be founded [Nyquist]

	// Frame's sizes and lapses
	public static final int FRAME_SIZE = 1102; //4410; 
	public static final int FRAME_INIT_LAPSE = 1025; //4100;
	public static final int SUBFRAME_SIZE = 110; //441;
	public static final int SUBFRAME_INIT_LAPSE = 55; //220;

	// Preemphasis alpha in order to smooth signal
	public static final float PREEMPHASIZE_ALPHA = 0.95f;

	// Total filters and useful cepstrals with and without DC(first filter)
	public static final int NUM_FILTERS = 26;
	public static final int NUM_CEPSTRALS = 13;
	public static final int NUM_CEPSTRALS_WITHOUT_DC = 12;

	// Number of elements for Fast Fourier Transform
	public static final int NFTT = 512;


}