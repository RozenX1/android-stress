package mfcc;

public class MFCC {
	private int numFilters; // we will use 20 filters by default
	private int numCepstrals; // 13 coefficients: [0,12], but later we will remove DC component MFCC(0)
	private int Fs;
	private int nfft;
	private int lowerFreqHz;
	private int higherFreqHz;
	private float[] frameSignal;
	private FFT fft;
	private DCT dct;

	// Constructor:
	public MFCC(int numFilters, int numCepstrals, int Fs, int nfft,
			int lowerFreqHz, float[] frameSignal) {
		this.numFilters = numFilters;
		this.numCepstrals = numCepstrals;
		this.Fs = Fs;
		this.nfft = nfft;
		this.lowerFreqHz = lowerFreqHz;
		this.higherFreqHz = this.Fs / 2;
		this.frameSignal = frameSignal;
		this.fft = new FFT();
        this.dct = new DCT(this.numCepstrals, numFilters);
	}

	/**
	 * Mel to Hz
	 */
	private float mel2Hz(float mel) {
		return 700f * (float) (Math.pow(10, mel / 2595) - 1f);
	}

	/**
	 * Hz to Mel
	 */
	private float hz2Mel(float freq) {
		return 2595f * (float) Math.log10(1 + freq / 700.0f);
	}

	/**
	 * Hamming window
	 */
	private void windowingFrameSignal() {
		// Make a hamming window:
		// w(n)=0.54 - 0.46 cos{(2*PI*n)/(size-1)}
		float TWO_PI = (float) (Math.PI * 2); //Constant
		for (int i = 0; i < frameSignal.length; i++)
			frameSignal[i] *= ( 0.54f - 0.46f * Math.cos(TWO_PI * i / (frameSignal.length - 1)) );
	}

	/**
	 * Zero padding
	 */
	private void zeroPadding() {
		float[] frameSignalPadded = new float[nfft];
		int i;
		for (i = 0; i < frameSignal.length; i++) {
			frameSignalPadded[i] = frameSignal[i];
		}
		for (; i < nfft; i++) {
			frameSignalPadded[i] = 0f;
		}
		frameSignal = frameSignalPadded;
	}

	private float[] magnitudeSpectrum() {
		float magSpectrum[] = new float[nfft];
		// calculate FFT for current frame
		fft.computeFFT(frameSignal);
		// calculate magnitude spectrum
		for (int i = 0; i < nfft; i++) {
			magSpectrum[i] = (float) Math.sqrt(Math.pow(fft.real[i], 2)
					+ Math.pow(fft.imag[i], 2));
		}
		return magSpectrum;
	}

	private float[] powerSpectrum() {
		float powSpectrum[] = magnitudeSpectrum();
		float AUX = 1.0f / nfft; //Constant
		for (int i = 0; i < nfft; i++) {
			// (pow^2)/nfft
			powSpectrum[i] = AUX * powSpectrum[i]*powSpectrum[i];
		}
		return powSpectrum;
	}

	//(not used at the moment)
	private float logEnergy(float pspec []) {
		float sum=0f;
		for(int i=0; i<pspec.length; i++){
			sum+=pspec[i];
		}
		return (float) Math.log(sum);
	}
	
	private float[] melPoints(float lowMel, float highMel) { // mels points to build filters
		float points[] = new float[numFilters + 2];
		float AUX = (highMel - lowMel) / (numFilters + 1); // Constant
		for (int i = 0; i < points.length; i++) {
			points[i] = lowMel + AUX * i;
		}
		return points;
	}

	private float[][] filterBanks() {
		//Each row -> one filter
		float lowMel = hz2Mel(lowerFreqHz);
		float highMel = hz2Mel(higherFreqHz);
		float[] mPoints = melPoints(lowMel, highMel);
		int bin[]=new int[mPoints.length];
		for(int i=0; i<mPoints.length; i++){
			bin[i]=(int) Math.floor((nfft+1) * mel2Hz(mPoints[i])/Fs);
		}
		float[][]fbank = new float[numFilters][nfft/2+1];
		for(int j=0; j<numFilters; j++){
			// Upward slope of the straight
			for(int i=bin[j]; i<bin[j+1]; i++ ){
				fbank[j][i] = (float)(i - bin[j]) / (float)(bin[j+1]-bin[j]);
			}
			// Downward slope of the straight
			for(int i=bin[j+1]; i<bin[j+2]; i++ ){
				fbank[j][i] = (float)(bin[j+2]-i) / (float)(bin[j+2]-bin[j+1]);
			}			
		}
		return fbank;
	}
	
	private float[] fbEnergies(float [] pspec, float[][] fb ){
		float feat[] = new float[numFilters];
		int HALF_NFTT = nfft/2 + 1; // Constant
		for(int i=0; i<numFilters; i++){
			for(int j=0; j<HALF_NFTT; j++){
				 feat[i]+= pspec[j] * fb[i][j];		
			}
			feat[i]= (float) Math.log(feat[i]);
		}
		return feat;
	}
	
	private float[] removeDC(float[] cepc){
		float[] res = new float[cepc.length-1];
		for(int i=0; i<res.length; i++){
			res[i] = cepc[i+1];
		}
		return res;
	}

	public float[] calcMFCC() {
		// Windowing signal:
		windowingFrameSignal();
		// Zero padding signal (if necessary to get power of two):
		zeroPadding();
		// Periodogram-based power spectral:
		float [] pspec = powerSpectrum();
		// Build FilterBanks:
		float[][] fb= filterBanks();
		// Compute the filterbank energies & pass to log filterbank energies:
		float feat[] = fbEnergies(pspec, fb);
		// Mel Cepstral coefficients, by DCT:
        float cepc[] = dct.performDCT(feat);
        //Remove DC component MFCC-> [1,12]
        cepc = removeDC(cepc);
        return cepc;
	}

}
