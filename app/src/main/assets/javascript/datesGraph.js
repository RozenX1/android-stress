$(function () {
  var dataStress = JSON.parse(DataStress.getJSON());
  var dataDuration = JSON.parse(DataDuration.getJSON());
  for(var i=0; i<dataStress.length; i++){
    dataStress[i].x = new Date(dataStress[i].x);
    dataDuration[i].x = new Date(dataDuration[i].x);
  }

  var chart = new CanvasJS.Chart("datesGraph", {
    animationEnabled: true,
    zoomEnabled: true,
    theme: "theme3",
    backgroundColor: "#cde5df",
    axisY:{
      gridColor: "#ab9ba9",
      labelFontColor:"#369EAD"
    },
    axisY2:{
      labelFontColor: "#C24642"
    },
    data: [
      {
        type: "spline",
        showInLegend: true,
        legendText: "Stress(%)",
        dataPoints: dataStress
      },
      {
        type: "spline",
        legendText: "Duration(sec)",
        axisYType: "secondary",
        showInLegend: true,
        dataPoints: dataDuration
      }
    ],
  });

  chart.render();
});
