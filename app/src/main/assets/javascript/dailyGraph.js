$(function () {
  var statistics = JSON.parse(DailyStatistics.getJSON());
  var dataPoints = [];
  for(var i in statistics){
    data = {y: statistics[i].percentage*statistics[i].duration, label: i+"h "};
    dataPoints.push(data);
  }

  var chart = new CanvasJS.Chart("dailyGraph", {
    animationEnabled: true,
    theme: "theme2",
    backgroundColor: "#cde5df",
    toolTipContent: "{label}: {y} secs - <strong>#percent</strong>",
    data: [
      {
        type: "doughnut",
        dataPoints: dataPoints,
        click: function(e){
          e.dataSeries.dataPoints.map(function(el){
          	el.exploded = false;
          })
          e.dataPoint.exploded = true;
          var hour = e.dataPoint.label;
          hour = hour.substring(0, hour.length-2);
          DailyStatistics.changeFilter(parseInt(hour));

        }
      },
    ],
  });

  chart.render();
});
