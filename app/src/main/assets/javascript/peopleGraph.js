$(function(){

  // Color gradient
  var stressColor = "#FF5333", relaxColor = "#009ACD";
  var hexToRGB = function(hex){
    var bigint = parseInt(hex.substring(1), 16);
    return {
      r: (bigint >> 16) & 255,
      g: (bigint >> 8) & 255,
      b: bigint & 255
    }
  };
  var stressRGB = hexToRGB(stressColor), relaxRGB = hexToRGB(relaxColor)
  var calcGradient = function(stress){
    var grad = function(comp){return parseInt(stress*stressRGB[comp] + (1 - stress)*relaxRGB[comp]);};
    return "rgb("+grad('r')+","+grad('g')+","+grad('b')+")";
  }

  // Graph
  sigma.settings.labelThreshold = 0;
  var data = JSON.parse(SigmaData.getJSON());
  var sig = new sigma('container');
  var angle = 2*Math.PI / Object.keys(data).length;
  var sum = 0;
  // Center node
  sig.graph.addNode({
    id: "Me",
    size:0,
    x:0,
    y:0,
  })
  // Remaining nodes
  var radius = 3;
  for(var contact in data){
    // Contact node
    var vector = {
      x: radius*Math.cos(sum),
      y: radius*Math.sin(sum)
    };
    sig.graph.addNode({
      id: contact,
      label: contact+" ",
      size: 0.5,
      x: vector.x,
      y: vector.y,
      color: '#388f04'
    })
    .addEdge({
      id: "Me-"+contact,
      source: "Me",
      target: contact,
      color: "#52c306"
    });
    sum += angle;
    // Calls subnodes
    var calls = data[contact];
    var subsum = subangle = Math.PI/(calls.length+1);
    var normalvector = {
      x: -vector.y,
      y: vector.x
    }
    for(var i=0; i<calls.length;i++){
      var subvector = {
        x: (normalvector.x*Math.cos(-subsum) - normalvector.y*Math.sin(-subsum)) + vector.x,
        y: (normalvector.x*Math.sin(-subsum) + normalvector.y*Math.cos(-subsum)) + vector.y
      }
      var color = calcGradient(calls[i].stress);
      sig.graph.addNode({
        id: calls[i].timestamp,
        label: (calls[i].stress*100).toString().substring(0,4)+"% ",
        size: calls[i].stress,
        x: subvector.x,
        y: subvector.y,
        color: color
      })
      .addEdge({
        id: contact+"-"+calls[i].timestamp,
        source: contact,
        target: calls[i].timestamp,
        color: color
      });
      subsum += subangle;
    }
  };


  sig.refresh();
});
